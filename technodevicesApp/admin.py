from django.contrib import admin
from .models import Marca, Producto, Categoria, Contacto

# Register your models here.

class ProductoAdmin(admin.ModelAdmin):
    list_display = ["nombre", "precio", "descripcion", "nuevo","fecha_publicacion", "categoria", "marca", "imagen"]
    list_editable = ["precio"]
    search_fields = ["nombre"]
    list_filter = ["marca", "categoria", "nuevo"]
    list_per_page = 5



admin.site.register(Marca)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Categoria)
admin.site.register(Contacto)
