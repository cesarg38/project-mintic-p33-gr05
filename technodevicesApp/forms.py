from django import forms
from django.db import models
from django.db.models import fields
from django.forms import widgets
from .models import Contacto, Producto
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class ContactoForm(forms.ModelForm):

    #nombre = forms.CharField(widget= forms.TextInput(attrs= {"class": "form-control"}))

    class Meta:
        model = Contacto
        #fields = ["nombre", "correo", "tipo_consulta", "mensaje", "avisos"]
        fields = '__all__'  # para enviar todos



class ProductoForm(forms.ModelForm):

    class Meta:
        model = Producto
        fields = ["nombre", "marca", "precio", "descripcion", "categoria", "nuevo", "imagen"]

        widgets = {
            "fecha_publicacion": forms.SelectDateWidget()
        }


class CustomUserCreationForm(UserCreationForm):

    class Meta:

        model = User

        fields = ["username", "first_name", "last_name", "email", "password1", "password2"]
