from django.db import models
from django.db.models.base import Model
from datetime import datetime

# Create your models here.

class Marca(models.Model):
    nombre = models.CharField(max_length= 50)

    def __str__(self) -> str:
        return self.nombre

class Categoria(models.Model):
    nombre = models.CharField(max_length= 50)

    def __str__(self):
        return self.nombre

class Producto(models.Model):
    nombre = models.CharField(max_length= 50)
    marca = models.ForeignKey(Marca, on_delete= models.PROTECT)
    precio = models.IntegerField()
    descripcion = models.TextField()
    nuevo = models.BooleanField()
    categoria = models.ForeignKey(Categoria, on_delete= models.PROTECT)
    fecha_publicacion = models.DateField( default= datetime.now)
    imagen = models.ImageField(upload_to = "productos", null = True)

    def __str__(self):
        return self.nombre


opciones_consultas = [
    [0, "consulta"],
    [1, "reclamo"],
    [2, "sugerencia"],
    [3, "felicitaciones"]
    ]

class Contacto(models.Model):
    nombre = models.CharField(max_length= 50)
    correo = models.EmailField()
    tipo_consulta = models.IntegerField(choices= opciones_consultas)
    mensaje = models.TextField()
    avisos = models.BooleanField()

    def __str__(self):
        return self.nombre