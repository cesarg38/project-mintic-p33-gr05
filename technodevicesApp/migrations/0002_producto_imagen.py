# Generated by Django 3.2.7 on 2021-10-03 01:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('technodevicesApp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='imagen',
            field=models.ImageField(null=True, upload_to='productos'),
        ),
    ]
